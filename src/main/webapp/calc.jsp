<%-- 
    Document   : calc
    Created on : 2 Dec 2020, 14:21:37
    Author     : simon
--%>

<%@page import="com.simonwoodworth.javamavencalcinclassdemo.Calculator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello IS3313!</h1>
        <p>This is to demonstrate changes after the first commit and push.</p>
        <p><%= new java.util.Date() %></p>
        <p><%= new Calculator().add(7, 5) %></p>
        <p><%= new Calculator().subtract(7, 5) %></p>
        <p><%= new Calculator().multiply(7, 5) %></p>
        <p><%= new Calculator().divide(7, 5) %></p>
        <p><%= new Calculator().square(7) %></p>
        <p><%= new Calculator().cube(7) %></p>
        <p><%= new Calculator().modulo(7, 5) %></p>

        <h1>118455906</h1>
        <h2>Your code demonstrating correct operation of the four new methods goes below.</h2>
        <h5>Add 7, 7 & 7 </h5>
        <p><%= new Calculator().addThree(7, 7, 7) %></p>
        <h5>Subtract 7 from 21 and subtract 7 from the result </h5>
        <p><%= new Calculator().subtractThree(21, 7, 7) %></p>
        <h5>Multiply 7, 7 & 7 </h5>
        <p><%= new Calculator().multiplyThree(7, 7, 7) %></p>
        <h5>Divide 16 by 2 and divide the answer by 4 </h5>
        <p><%= new Calculator().divideThree(16, 2, 4) %></p>
        <h2>End of your code</h2>
    </body>
</html>
